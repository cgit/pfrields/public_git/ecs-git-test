CONTENT     = f
MAJOR       = 15

PRIOR_RELEASE = f-14

# Custom reports for Fedora (different from internal Red Hat reports)
# keep them in PWD because reports change for each release.
# This is an actual file and NOT a symlink to ../../common/reports.tji
# which is automatically created by Makefile.common if it does not exist
REPORT   = ./reports.tji

# Red Hat Master Make File
include ../../common/Makefile.common

# Fedora Specific File
include ./Makefile.custom

