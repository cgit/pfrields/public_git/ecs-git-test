<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

  <xsl:output method="html"/>

  <xsl:param name="title">Combined Documentation &amp; Release Engineering Tasks</xsl:param>

  <xsl:template match="/">

    <xsl:variable name="htitle" select="concat (Project/Name, ' ', Project/Version, ' ', $title)"/>
    <html>
      <head>
        <title><xsl:value-of select="$htitle"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      </head>
      <body>
        <h1><xsl:value-of select="$htitle"/></h1>
        <table align="center" cellpadding="2" style="background-color:#000000">
          <thead>
            <!-- top border -->
            <tr valign="middle" style="background-color:#8989b2; font-size:110%; font-weight:bold; text-align:center">
              <td rowspan="2">No.</td>
              <td rowspan="2">Name</td>
              <td rowspan="2">Start</td>
              <td rowspan="2">End</td>
              <td rowspan="2">Duration</td>
            </tr>

          </thead>

          <tbody>
            <xsl:for-each select="//Task[Flag = 'design' or Flag='key']">
	      <xsl:sort select="actualStart"/>

	      <xsl:if test="count(self::node()[Flag='design']) + count(self::node()[Flag='key'])">
		<xsl:element name="tr">
		  <xsl:attribute name="valign">middle</xsl:attribute>
		  <xsl:attribute name="style">
		    <xsl:choose>

                      <!-- design color == #89b2b2                         -->
                      <!-- key color == #b2b289                  -->
                      <!-- design & key share same task == #8989b2 -->

                      <!-- Both share same task -->
		      <xsl:when test="count(self::node()[Flag='design']) + count(self::node()[Flag='key']) &gt; 1">background-color: #8989b2;</xsl:when>
                      <!-- design only task -->
		      <xsl:when test="count(self::node()[Flag='key']) = 0">background-color: #89b2b2;</xsl:when>

                      <!-- key only -->
		      <xsl:otherwise>background-color: #b2b289;</xsl:otherwise>
		    </xsl:choose>
		  </xsl:attribute>
		  <td style="text-align:center; "><xsl:value-of select="position()"/></td>
		  <td nowrap="nowrap" style="text-align:left; padding-left:2px; font-size:120%"><xsl:value-of select="Name"/></td>
		  <xsl:choose>
		    <xsl:when test="actualEnd = actualStart">
		      <td style="text-align:center; font-size:90%;" colspan="3"><xsl:value-of select="substring-before(actualStart/@humanReadable, ' ')"/></td>
		    </xsl:when>
		    <xsl:otherwise>
		      <td style="text-align:left; font-size:90%; "><xsl:value-of select="substring-before(actualStart/@humanReadable, ' ')"/></td>

		      <td style="text-align:left; font-size:90%; "><xsl:value-of select="substring-before(actualEnd/@humanReadable, ' ')"/></td>
		      <td style="text-align:right; font-size:90%; ">
			<xsl:value-of select="round ((actualEnd - actualStart) div 864) div 100"/>
		      </td>
		    </xsl:otherwise>
		  </xsl:choose>
		</xsl:element>
	      </xsl:if>

	    </xsl:for-each>
          </tbody>
        </table>

	<p><span style="font-size:0.7em">Created with with <a HREF="http://www.taskjuggler.org">TaskJuggler</a> 
           and <b>combine-design-key.xsl</b></span></p>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
