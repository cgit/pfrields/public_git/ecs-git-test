#!/usr/bin/python

# created by James Laska <jlaska@redhat.com>
# modified by John Poelstra <poelstra@redhat.com>

# This script parses csv files created with a custom report by TaskJuggler 
# It is used to send reminders of upcoming tasks to the team lists

import os
import sys

if len(sys.argv) == 2:
    fname = sys.argv[1]
else:
    print "Usage: %s <filename>" % sys.argv[0]
    sys.exit(1)

#print "   Start               End                        Name"
fd = open(fname, "r")
for line in fd.readlines():
    #line.strip()
    (start, end, name) = line[:-1].split(";")

    # remove quotes
    name = name.replace('"', '')
    start = start.replace('"', '')
    end = end.replace('"', '')
    print "%-11s %-11s %s" % (start, end, name )
fd.close()
